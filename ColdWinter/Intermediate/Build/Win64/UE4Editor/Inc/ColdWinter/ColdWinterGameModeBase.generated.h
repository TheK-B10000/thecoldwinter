// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COLDWINTER_ColdWinterGameModeBase_generated_h
#error "ColdWinterGameModeBase.generated.h already included, missing '#pragma once' in ColdWinterGameModeBase.h"
#endif
#define COLDWINTER_ColdWinterGameModeBase_generated_h

#define ColdWinter_Source_ColdWinter_ColdWinterGameModeBase_h_15_RPC_WRAPPERS
#define ColdWinter_Source_ColdWinter_ColdWinterGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define ColdWinter_Source_ColdWinter_ColdWinterGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAColdWinterGameModeBase(); \
	friend COLDWINTER_API class UClass* Z_Construct_UClass_AColdWinterGameModeBase(); \
public: \
	DECLARE_CLASS(AColdWinterGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/ColdWinter"), NO_API) \
	DECLARE_SERIALIZER(AColdWinterGameModeBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define ColdWinter_Source_ColdWinter_ColdWinterGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAColdWinterGameModeBase(); \
	friend COLDWINTER_API class UClass* Z_Construct_UClass_AColdWinterGameModeBase(); \
public: \
	DECLARE_CLASS(AColdWinterGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/ColdWinter"), NO_API) \
	DECLARE_SERIALIZER(AColdWinterGameModeBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define ColdWinter_Source_ColdWinter_ColdWinterGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AColdWinterGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AColdWinterGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AColdWinterGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AColdWinterGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AColdWinterGameModeBase(AColdWinterGameModeBase&&); \
	NO_API AColdWinterGameModeBase(const AColdWinterGameModeBase&); \
public:


#define ColdWinter_Source_ColdWinter_ColdWinterGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AColdWinterGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AColdWinterGameModeBase(AColdWinterGameModeBase&&); \
	NO_API AColdWinterGameModeBase(const AColdWinterGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AColdWinterGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AColdWinterGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AColdWinterGameModeBase)


#define ColdWinter_Source_ColdWinter_ColdWinterGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define ColdWinter_Source_ColdWinter_ColdWinterGameModeBase_h_12_PROLOG
#define ColdWinter_Source_ColdWinter_ColdWinterGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ColdWinter_Source_ColdWinter_ColdWinterGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	ColdWinter_Source_ColdWinter_ColdWinterGameModeBase_h_15_RPC_WRAPPERS \
	ColdWinter_Source_ColdWinter_ColdWinterGameModeBase_h_15_INCLASS \
	ColdWinter_Source_ColdWinter_ColdWinterGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ColdWinter_Source_ColdWinter_ColdWinterGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ColdWinter_Source_ColdWinter_ColdWinterGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	ColdWinter_Source_ColdWinter_ColdWinterGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	ColdWinter_Source_ColdWinter_ColdWinterGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	ColdWinter_Source_ColdWinter_ColdWinterGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ColdWinter_Source_ColdWinter_ColdWinterGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
